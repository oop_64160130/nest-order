import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/product/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Customer)
    private customersReposiyory: Repository<Customer>,
    @InjectRepository(Product)
    private productReposiyory: Repository<Product>,
    @InjectRepository(OrderItem)
    private orderItemReposiyory: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const customer = await this.customersReposiyory.findOneBy({
      id: createOrderDto.customerId,
    });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.orderRepository.save(order);
    // const orderItems: OrderItem[] = await Promise.all(
    //   createOrderDto.orderItems.map(async (od) => {
    //     const orderItem = new OrderItem();
    //     orderItem.amount = od.amount;
    //     orderItem.product = await this.productReposiyory.findOneBy({
    //       id: od.productId,
    //     });
    //     orderItem.name = orderItem.product.name;
    //     orderItem.price = orderItem.product.price;
    //     orderItem.total = orderItem.product.price * orderItem.amount;
    //     orderItem.order = order;
    //     return orderItem;
    //   }),
    // );
    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.product = await this.productReposiyory.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.total = orderItem.product.price * orderItem.amount;
      orderItem.order = order;
      await this.orderItemReposiyory.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }
    await this.orderRepository.save(order);
    return await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.orderRepository.find({ relations: ['customer', 'orderItems'] });
  }

  findOne(id: number) {
    return this.orderRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOneBy({ id: id });
    return this.orderRepository.softRemove(order);
  }
}
